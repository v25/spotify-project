from flask import render_template, redirect, url_for, flash, Blueprint, session
from flask_login import login_user, logout_user, current_user
from music_app.forms import LoginForm, RegistrationForm
from music_app import db
from music_app.models import User


bp = Blueprint('auth', __name__, )


@bp.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('routes.index'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()

        """ The following conditional is probably unnecessary since
        LoginForm validators for username & password were added. """
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('auth.login'))

        if form.shared.data:
            session['shared'] = True
        else:
            session['shared'] = False
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('routes.index'))

    return render_template('login.html', title='Sign In', form=form)


@bp.route('/logout')
def logout():
    if session.get('shared'):
        session.clear()
        logout_user()
        return redirect(url_for('routes.logout_spotify'))
    else:
        session.clear()
        logout_user()
        return redirect(url_for('routes.index'))


@bp.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('routes.index'))

    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Success! You are now a registered user.')
        return redirect(url_for('auth.login'))

    return render_template('register.html', title='Register', form=form)
