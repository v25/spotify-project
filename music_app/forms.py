from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import ValidationError, DataRequired, EqualTo, Length
from flask_wtf.file import FileField, FileAllowed
from music_app.models import User


class UploadForm(FlaskForm):
    upload = FileField('Select a Playlist File', validators=[
        FileAllowed(['xml', 'txt', 'csv'])
        ])
    submit = SubmitField('Upload')

    def validate_upload(self, upload):
        if not upload.data:
            raise ValidationError('You must select a file.')


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Stay logged in')
    shared = BooleanField('shared or public computer')
    submit = SubmitField('Sign In')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if not user:
            raise ValidationError('That username is not registered.')

    def validate_password(self, password):
        user = User.query.filter_by(username=self.username.data).first()
        if user and not user.check_password(password.data):
            raise ValidationError('Incorrect password')


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField(
        'Password', validators=[DataRequired(), Length(min=8)])
    password2 = PasswordField(
        'Confirm Password',
        validators=[DataRequired(), EqualTo('password')]
        )
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError(
                '''That name is already in use. Please choose a different
                username.'''
            )
