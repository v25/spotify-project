from flask import Flask
# from config import Config
import logging
from logging.handlers import RotatingFileHandler
import os
from secrets import token_urlsafe
from flask_sqlalchemy import SQLAlchemy
# from flask_migrate import Migrate
from flask_login import LoginManager

project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
fallback_key = token_urlsafe(32)

db = SQLAlchemy()
# migrate = Migrate()
login = LoginManager()
login.login_view = 'auth.login'
login.login_message = 'Please log in to access this page.'


def create_app():
    """Create and configure the app.

    Need to configure database uri w/ driiver,
    e.g., 'mysql+mysqlconnector://admin.../db'.
    See https://docs.sqlalchemy.org/en/13/dialects/mysql.html#dialect-mysql
    """
    app = Flask(__name__)
    app.config.from_mapping(
        SECRET_KEY=os.getenv('SECRET_KEY', fallback_key),
        SQLALCHEMY_DATABASE_URI=os.getenv('SPOTIFY_DB'),
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        SQLALCHEMY_ENGINE_OPTIONS={'pool_recycle': 280},
        SESSION_COOKIE_SAMESITE="None",
        SESSION_COOKIE_SECURE=True,
    )

    db.init_app(app)
    # migrate.init_app(app, db)
    login.init_app(app)

    temp_dir = os.path.join(project_dir, 'tmp')
    if not os.path.exists(temp_dir):
        os.mkdir(temp_dir)

    app.config['TEMP_DIR'] = temp_dir

    if not app.debug and not app.testing:
        logs_dir = os.path.join(project_dir, 'logs')
        if not os.path.exists(logs_dir):
            os.mkdir(logs_dir)
        file_handler = RotatingFileHandler(
            os.path.join(logs_dir, 'spotify-project.log'),
            maxBytes=10240,
            backupCount=10
            )
        file_handler.setFormatter(logging.Formatter(
            '''
            %(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]
            '''
            )
        )
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)
        app.logger.setLevel(logging.INFO)
        app.logger.info('Spotify Project startup')

    from . import routes
    app.register_blueprint(routes.bp)

    from . import auth
    app.register_blueprint(auth.bp, url_prefix='/auth')

    from . import errors
    app.register_blueprint(errors.bp)

    return app
