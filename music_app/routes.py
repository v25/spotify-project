from flask import render_template, request, url_for, redirect, Blueprint, \
    session, json, flash, current_app, send_from_directory
from .spotify import SpotifyHelper, ParsePlaylists
from music_app.forms import UploadForm
import os.path
from flask_login import login_required, current_user
from music_app import db
from datetime import datetime
from secrets import token_urlsafe
from more_itertools import chunked
from tempfile import NamedTemporaryFile
import csv
import re
import chardet

bp = Blueprint('routes', __name__, static_folder='./static')

sh = SpotifyHelper()
p = ParsePlaylists()
redirect_url = sh.REDIR_URL
session_expired = '''
    Your session has expired. Click "Start Your Session" to start a new one.'''
default_state = os.getenv('SPOTIFY_DEFAULT_STATE')


def get_endpoint(key):
    with open(os.path.join(bp.static_folder, 'JSON/endpoints.json')) as f:
        d = json.load(f)
    return d[key]["url"]


def list_extractor(list, key):
    """Extract a list of values from a single key in list of dictionaries."""
    values = [item[key] for item in list]
    return values


@bp.route('/')
@bp.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html')


@bp.route('/action', methods=['GET', 'POST'])
@login_required
def action():
    if current_user.is_authenticated:
        if current_user.state == default_state:
            current_user.state = token_urlsafe(12)
            db.session.commit()
        ts = current_user.token_json
        tct = current_user.token_creation_time
        status = sh.token_status(token_string=ts, token_creation_time=tct)

        if (session.get('shared')
                or status == 'expired'
                or status == 'no_token'):
            return redirect(url_for('routes.new_request'))
        else:
            return redirect(url_for('routes.profile'))
    else:
        return redirect(url_for('routes.index'))


@bp.route('/new_request', methods=['GET', 'POST'])
@login_required
def new_request():
    params = sh.AUTH_DICT
    params['state'] = current_user.state
    url = sh.auth_request(**params)
    return redirect(url)


@bp.route('/callback', methods=['GET', 'POST'])
@login_required
def callback():
    # Check that 'state' strings match.
    if current_user.state != request.args.get('state'):
        raise Exception('Cross-site request forgery')

    # Store the code so ProfileData.token_header() can access it.
    if request.args.get('code'):
        session['auth_code'] = request.args.get('code')
        return redirect(url_for('routes.profile'))
    else:
        flash('Something went wrong.')
        return redirect(url_for('routes.index'))


def token_header():
    ts = current_user.token_json
    tct = current_user.token_creation_time
    code = session.get('auth_code')
    if code:
        session['auth_code'] = ""  # Clear this after code variable is set.
    try:
        tj = sh.token_json(
            token_string=ts, token_creation_time=tct, code=code)
        if str(tj) != ts:
            current_user.token_json = str(tj)
            current_user.token_creation_time = datetime.utcnow()
            db.session.commit()
        token = tj['access_token']
        token_type = tj['token_type']
        header = {'Authorization': token_type + ' ' + token}
        return header
    except KeyError:
        current_app.logger.exception('Token expired')
        return None


class ProfileData:
    """Collect data to populate the 'profile' view."""
    def __init__(self):
        self.header = token_header()

    def mock_token_header(self):  # Pytest (../tests/conftest.py) needs this.
        pass

    def profile_(self):
        return sh.get_data(
            self.header,
            get_endpoint("get-current-user-profile")
            )

    def playlists_(self, limit=20, offset=0):
        url = (get_endpoint("get-list-of-current-user-playlists")
               + "?limit="
               + str(limit)
               + "&offset="
               + str(offset))
        return sh.get_data(self.header, url)

    def all_playlists_(self):

        # Make the first request as standard
        url = get_endpoint("get-list-of-current-user-playlists")

        playlists = []

        while True:
            result = sh.get_data(self.header, url)
            print (url)

            if 'items' not in result:
                break # Deal with an empty result set
            else:
                for item in result['items']:
                    playlists.append(item)

            if result['next'] is None:
                break
            else:
                url = result['next']

        return playlists

    def history_(self):
        return sh.get_data(
            self.header,
            get_endpoint("get-current-user-recently-played-tracks")
            )

    def top_artists_(self):
        # Build endpoint url for top artists.
        top_a_url = get_endpoint(
            "get-user-top-artists+tracks").format(type="artists")
        top_a_url = top_a_url + "?limit=50&time_range=long_term"

        return sh.get_data(self.header, top_a_url)

    def top_tracks_(self):
        # Build endpoint url for top tracks.
        top_t_url = get_endpoint(
            "get-user-top-artists+tracks").format(type="tracks")
        top_t_url = top_t_url + "?limit=50&time_range=long_term"

        return sh.get_data(self.header, top_t_url)


@bp.before_request
def header_check():
    if request.path is ('/profile'
                        or '/upload'
                        or '/retry'
                        or '/download'
                        or '/playlists'):
        header = token_header()
        if not header and not current_app.testing:
            flash(session_expired)
            return redirect(url_for('routes.index'))


@bp.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    """Snapshot page of current user's profile.

    Displays playlists, recently played tracks,
    top artists, and tops tracks.
    """
    pd = ProfileData()
    missing = session.get('missing')
    retry_missing = session.get('retry_missing')
    with open(os.path.join(bp.static_folder, 'JSON/mock.json')) as f:
        mock = json.load(f)
    err_msg = 'We could not retrieve '

    try:
        profile = pd.profile_()
    except Exception as e:
        current_app.logger.exception(f'{e}: Could not retrieve user profile.')
        profile = mock['profile']
        flash(err_msg + 'user profile.')
    session["user"] = profile

    try:
        playlists = pd.playlists_()
    except Exception as e:
        current_app.logger.exception(f'{e}: Could not retrieve playlists.')
        playlists = mock['playlists']
        flash(err_msg + 'playlists.')
    session['playlists'] = []
    playlist_names = [item['name'] for item in playlists['items']]
    session['playlists'] = playlist_names

    try:
        history = pd.history_()
    except Exception as e:
        current_app.logger.exception(
            f'{e}: Could not retrieve recently played songs.')
        history = mock['history']
        flash(err_msg + 'recently played songs.')

    try:
        top_artists = pd.top_artists_()
    except Exception as e:
        current_app.logger.exception(f'{e}: Could not retrieve top artists.')
        top_artists = mock['top_artists']
        flash(err_msg + 'your top artists.')

    try:
        top_tracks = pd.top_tracks_()
    except Exception as e:
        current_app.logger.exception(f'{e}: Could not retrieve top tracks.')
        top_tracks = mock['top_tracks']
        flash(err_msg + 'your top tracks.')

    return render_template(
        'profile.html',
        playlists=playlists['items'],
        history=history['items'],
        top_artists=top_artists['items'],
        top_tracks=top_tracks['items'],
        list_extractor=list_extractor,
        missing=missing,
        retry_missing=retry_missing,
        title='Playlist-ify | Your Spotify Music'
        )


@bp.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    header = token_header()

    # Clear 'missing' and 'retry_missing' from session.
    session['missing'] = []
    session['retry_missing'] = []

    # Upload the playlist file.
    form = UploadForm()
    err_tracks = []
    message = ""
    if form.validate_on_submit():
        # Use the file name as the playlist name.
        file = form.upload.data
        filename = file.filename
        playlist_name = filename.split('.')[0]

        # Parse an xml playlist file.
        if filename.split('.')[-1] == 'xml':
            songs = p.parse_songs_xml(file)

        # Parse a csv or txt (actually tab-delimited csv) file.
        else:
            # Start with bytes, then encode.
            byte_str = file.read()
            try:
                u_string = byte_str.decode()
            except Exception as e:
                encoding = chardet.detect(byte_str)['encoding'].lower()
                u_string = byte_str.decode(encoding=encoding)
                current_app.logger.exception(e)
            songs = p.parse_songs_csv(u_string)

        # Create new playlist if it does not already exist.
        if playlist_name not in session['playlists']:
            params = {"name": playlist_name, "public": False}

            # Get and format endpoint URL from static/JSON/endpoints.json.
            url_create = get_endpoint(
                "create-playlist").format(user_id=session["user"]["id"])

            # Create the playlist, check if `songs` available in Spotify.
            new_playlist = sh.post_data(header, url_create, **params)
            if new_playlist[0] in [200, 201]:
                playlist_id = new_playlist[1]['id']
                tracks = sh.collect_tracks(
                    header, session['user']['country'], *songs)

    # Log and flash errors due to tracks with bad data.  See err_string below.

                for error in tracks['errors']:
                    current_app.logger.exception(error)
                    error_track = re.findall(
                        r"{'name': '(.+)', 'artist'", error)
                    err_tracks += error_track

                found = tracks['found']
                missing_tracks = tracks['missing']
                d = {
                    "playlist_id": playlist_id,
                    "playlist_name": playlist_name,
                    "tracks": []
                }
                if found["uris"]:
                    # Format endpoint URL from static/JSON/endpoints.json.
                    url_add = get_endpoint("add-tracks-to-playlist").format(
                        playlist_id=playlist_id)

                    # Maximum of 100 tracks can be added at once.
                    if len(found["uris"]) <= 100:
                        add_tracks = sh.post_data(header, url_add, **found)
                        if add_tracks[0] in [200, 201]:
                            message = "Success! Your playlist was added. "

                    # For more than 100 tracks, process in chunks.
                    else:
                        count = 0
                        chunks = chunked(found["uris"], 100)
                        for chunk in chunks:
                            found_chunk = {}
                            found_chunk["uris"] = chunk
                            add_tracks = sh.post_data(
                                header, url_add, **found_chunk)
                            if add_tracks[0] in [200, 201]:
                                count += 1
                        if count > 0:
                            message = "Success! Your playlist was added. "

                # If playlist created, but no tracks were found in Spotify.
                else:
                    message = '''
                        We created a playlist but couldn't add any tracks.'''

                # List tracks that were not found in Spotify.
                if missing_tracks:
                    missing_list = [
                        list(song.values()) for song in missing_tracks]
                    missing = [(' | ').join(song) for song in missing_list]
                    d["tracks"] = missing

                    # Avoid repeat entries for session.missing playlists.
                    names = [i['playlist_name'] for i in session['missing']]
                    if playlist_name not in names:
                        session['missing'].append(d)

                    if found["uris"]:
                        message = message + "We couldn't find some tracks."
            else:
                message = "Something went wrong -- playlist not created."
        else:
            message = "You already have a playlist by that name."
        if err_tracks:
            err_str = "Tracks skipped due to data errors: "
            err_str += ', '.join(err_tracks)
            flash(err_str)
        flash(message)
        return redirect(url_for('routes.profile'))
    else:
        return render_template(
            'upload.html',
            form=form,
            title='Playlist-ify | Import a Playlist'
            )


@bp.route('/retry', methods=['GET', 'POST'])
@login_required
def retry():
    """Try to match missing tracks by just using name and artist."""
    header = token_header()
    retry_missing = []
    for playlist in session['missing']:
        songs = []
        playlist_id = playlist['playlist_id']
        playlist_name = playlist['playlist_name']

        # Make a dictionary from string of missing tracks.
        track_list = [track.split(' | ') for track in playlist['tracks']]
        for track in track_list:
            tracks_dict = {}
            tracks_dict['name'] = track[0]
            tracks_dict['artist'] = track[1]
            songs.append(tracks_dict)  # Append track_dict to songs list.
        tracks = sh.collect_tracks(
            header,
            session['user']['country'],
            *songs,
            include_album=False,
            filter_artist=True  # Replaces '&' with a comma.
            )
        found = tracks['found']
        if found["uris"]:
            success = "Success! We added tracks to " + playlist_name + "."
            url_add = get_endpoint("add-tracks-to-playlist").format(
                playlist_id=playlist_id)
            if len(found["uris"]) <= 100:
                more_added = sh.post_data(header, url_add, **found)
                if more_added[0] in [200, 201]:
                    flash(success)
            else:
                messages = []
                chunks = chunked(found["uris"], 100)
                for chunk in chunks:
                    found_chunk = {}
                    found_chunk["uris"] = chunk
                    more_added = sh.post_data(
                        header, url_add, **found_chunk)
                    if more_added[0] in [200, 201]:
                        messages.append(success)
                if success in messages:
                    flash(success)
        if tracks['missing']:
            missing_list = [
                list(song.values()) for song in tracks['missing']]
            missing = [(' | ').join(song) for song in missing_list]
            d = {
                "playlist_id": playlist_id,
                "playlist_name": playlist_name,
                "tracks": missing
                }
            retry_missing.append(d)
            flash("Some tracks are still missing from " + playlist_name + ".")
    session['retry_missing'] = retry_missing
    session['missing'] = []  # Clear missing list.
    return redirect(url_for('routes.profile'))


@bp.route('/download', methods=['GET'])
@login_required
def download(backup=False):
    if not request.referrer:
        flash("You can only access the download page from here.")
        return redirect(url_for('routes.profile'))
    header = token_header()
    temp_dir = current_app.config['TEMP_DIR']
    params = {}
    params["market"] = session['user']['country']
    if request.args.get('backup'):
        backup = True

    if not backup:  # For playlist downloads
        playlist_id = request.args.get('id')
        file_name = request.args.get('name') + '.csv'
        url = get_endpoint("get-playlist-tracks").format(
            playlist_id=playlist_id)
        fields = ('items.track.name,'
                  + ' items.track.album.name,'
                  + ' items.track.artists')
        params["fields"] = fields
        query_limit = 100

    if backup:  # For download of 'Liked Songs'
        playlist_id = session['user']['display_name'] + '_liked_songs'
        file_name = 'Liked Songs.csv'
        url = get_endpoint("get-user-saved-tracks")
        query_limit = 50

    count = 0
    tracks = []

    while not len(tracks) % query_limit:
        params["limit"] = query_limit
        params["offset"] = query_limit * count
        section = sh.get_playlist_tracks(header, url, **params)
        tracks += section["items"]
        count += 1

    if not tracks:
        flash("Something went wrong. We couldn't get your playlist.")
        return redirect(url_for('routes.profile'))

    with NamedTemporaryFile(
                        mode='w+t',
                        encoding='utf8',
                        newline='',
                        prefix=playlist_id + '.',
                        dir=temp_dir,
                        delete=False,
                                        ) as csvfile:
        fieldnames = ['Name', 'Artist', 'Album']
        if backup:
            fieldnames += ['Date Added', 'Spotify ID']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='\t')
        writer.writeheader()
        for item in tracks:
            artists = ', '.join(
                list_extractor(item["track"]["artists"], "name"))
            rows = {
                    'Name': item["track"]["name"],
                    'Artist': artists,
                    'Album': item["track"]["album"]["name"]
                }
            if backup:
                rows['Date Added'] = item["added_at"]
                rows['Spotify ID'] = item["track"]["id"]
            writer.writerow(rows)

    # Clear old temp files
    t1 = datetime.now().timestamp()
    for file in os.listdir(temp_dir):
        file_path = os.path.join(temp_dir, file)
        t2 = os.path.getmtime(file_path)
        if (t1 - t2) > 300:
            os.remove(file_path)

    tmp_files = os.listdir(temp_dir)
    target = [file for file in tmp_files if file.startswith(playlist_id)]

    # Sort to ensure target[0] is the newest version.
    target.sort(
        key=lambda x: os.path.getmtime(os.path.join(temp_dir, x)),
        reverse=True
    )

    return send_from_directory(
        temp_dir,
        target[0],
        as_attachment=True,
        attachment_filename=file_name
    )


@bp.route('/logout_spotify', methods=['GET'])
def logout_spotify():
    logout_url = sh.spotify_logout()
    return redirect(logout_url)


@bp.route('/playlists', methods=['GET'])
@login_required
def playlists():

    pl = ProfileData()
    playlists = pl.all_playlists_()

    return render_template(
        'playlists.html',
        playlists=playlists,
        title='Playlists'
        )
