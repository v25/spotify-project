import requests
import os
import base64
from datetime import datetime
import xml.etree.ElementTree as ET
import re
import csv
from io import StringIO
from ast import literal_eval


class SpotifyHelper:
    BASE_URL = 'https://www.spotify.com'
    AUTH_URL = 'https://accounts.spotify.com/authorize'
    CLIENT_ID = os.getenv('SPOTIFY_CLIENT_ID')
    REDIR_URL = os.getenv('SPOTIFY_REDIRECT_URL')
    TOKEN_URL = 'https://accounts.spotify.com/api/token'
    ALL_SCOPES = [
        'playlist-read-collaborative',
        'playlist-modify-private',
        'user-modify-playback-state',
        'user-read-private',
        'user-library-modify',
        'user-follow-modify',
        'user-read-recently-played',
        'streaming',
        'user-read-currently-playing',
        'playlist-modify-public',
        'user-read-playback-state',
        'app-remote-control',
        'user-library-read',
        'user-follow-read',
        'user-read-email',
        'playlist-read-private',
        'user-top-read'
        ]
    # SCOPES are the scopes currently used by the app.
    SCOPES = [
        'playlist-read-collaborative',
        'playlist-modify-private',
        'user-read-private',
        'user-read-recently-played',
        'playlist-read-private',
        'user-top-read',
        'user-library-read'
        ]
    AUTH_DICT = {
        'client_id': CLIENT_ID,
        'response_type': 'code',
        'scope': ('%20').join(SCOPES),
        'redirect_uri': REDIR_URL
        }  # Needs state
    GET_TOKEN_DICT = {
        'grant_type': 'authorization_code',
        'redirect_uri': REDIR_URL
        }  # Needs code
    REFRESH_TOKEN_DICT = {
        'grant_type': 'refresh_token',
        'redirect_uri': REDIR_URL
        }  # Needs refresh_token
    TIMEOUT = (3.05, 27)

    def auth_header(self):
        client_str = self.CLIENT_ID + ':' + os.getenv('SPOTIFY_CLIENT_SECRET')
        encoded = base64.b64encode(client_str.encode())
        return {'Authorization': 'Basic %s' % encoded.decode()}

    def auth_request(self, **params):
        s = []
        for k, v in params.items():
            add_str = k + '=' + str(v)
            s.append(add_str)
            full = ('&').join(s)
        url = self.AUTH_URL + '?' + full
        return url

    def get_token(self, **params):
        header = self.auth_header()
        r = requests.post(
            self.TOKEN_URL, headers=header, data=params, timeout=self.TIMEOUT)
        return r.json()

    def token_status(self, token_string=None, token_creation_time=None):
        """Checks for presence and status of a stored token_string.

        Token is 'usable', 'refreshable', or 'expired' based on token age and
        presence or absence of 'refresh_token'.
        """
        if token_string and token_creation_time:
            age = datetime.utcnow() - token_creation_time
            age = age.total_seconds()
            d = literal_eval(token_string)
            if 'expires_in' in d.keys() and age < d['expires_in']:
                status = 'usable'
            elif all(
                    [x in d.keys() for x in ['expires_in', 'refresh_token']]
                    ) and age >= d['expires_in']:
                status = 'refreshable'
            else:
                status = 'expired'
        else:
            status = 'no_token'
        return status

    def token_json(
            self, token_string=None, token_creation_time=None, code=None):
        """Retrieves access token by one of three methods.

        Code following 'expired' or 'missing' status will only work with a
        fresh authorization code, i.e., '/index' > '/action' > '/callback'.
        Otherwise, views that use token_header() will
        raise 'token expired' Exception and flash(session_expired).
        """
        if not token_string:
            token_string = '{}'
        d = literal_eval(token_string)
        status = self.token_status(
            token_string=token_string, token_creation_time=token_creation_time)

        if status == 'usable':
            token_json = d

        if status == 'refreshable':
            params = self.REFRESH_TOKEN_DICT
            params['refresh_token'] = d['refresh_token']
            token_json = self.get_token(**params)

        if status == 'expired' or status == 'no_token':
            params = self.GET_TOKEN_DICT
            params['code'] = code
            token_json = self.get_token(**params)

        return token_json

    def get_data(self, header, url):
        r = requests.get(url, headers=header, timeout=self.TIMEOUT)
        return r.json()

    # Use 'json=' rather than 'data='
    def post_data(self, header, url, **params):
        r = requests.post(
            url, headers=header, json=params, timeout=self.TIMEOUT)
        return r.status_code, r.json()

    def collect_tracks(
            self, header, market, *songs,
            include_album=True, filter_artist=False):
        """Collects track uris from a search query

        From https://developer.spotify.com/documentation/web-api/reference/
        playlists/add-tracks-to-playlist/:
        For example:
        {"uris": ["spotify:track:4iV5W9uYEdYUVa79Axb7Rh",
        "spotify:track:1301WleyT98MSxVHPZCA6M"]}
        """
        track_uris = {}
        track_uris["uris"] = []
        missing = []
        errors = []
        for song in songs:
            try:
                # Apostrophes are not processed correctly by API
                name = song['name'].replace("'", "")
                artist = song['artist'].replace("'", "")
                if filter_artist:
                    artist = re.sub(r'\s&\s|,\s&\s', ', ', artist)
                query = 'track:' + name + ' artist:' + artist
                if include_album:
                    album = song['album'].replace("'", "")
                    # Remove parentheses or brackets, e.g., [Disc 1]
                    album = re.sub(r'\s\(.+\)$|\s\[.+\]$', '', album)
                    query = query + ' album:' + album
                params = {
                    'q': query,
                    'type': 'track',
                    'market': market,
                    'limit': 1
                    }
                r = requests.get(
                    "https://api.spotify.com/v1/search",
                    params=params,
                    headers=header
                    )
                results = r.json()['tracks']['items']

                ''' Allowing missing list to grow too big can lead to
                Werkzeug error: "cookie is too large".
                '''
                if results:
                    track_uris["uris"].append(results[0]['uri'])
                elif len(missing) < 200:
                    missing.append(song)
                    continue
                else:
                    continue
            except Exception as e:
                error = f'{e}: ' + str(song)
                if len(errors) < 50:
                    errors.append(error)
                continue
        d = {'found': track_uris, 'missing': missing, 'errors': errors}
        return d

    def get_playlist_tracks(self, header, url, **params):
        r = requests.get(
            url, headers=header, params=params, timeout=self.TIMEOUT)
        return r.json()

    def spotify_logout(self):
        r = requests.head(self.BASE_URL)
        localized = r.headers['location']
        logout_url = localized + 'logout'
        return logout_url


class ParsePlaylists:
    """Returns list of song dictionaries with name, artist, album"""

    def parse_songs_xml(self, file):
        """Parses iTunes playlist exported as xml file."""
        def lookup(d, data):
            found = False
            for child in d:
                if found:
                    return child.text
                if child.tag == 'key' and child.text == data:
                    found = True
            return None
        parsed_xml = ET.ElementTree(file=file)
        dict_elements = parsed_xml.findall('./dict/dict/dict')
        songs = []
        for elem in dict_elements:
            if not lookup(elem, 'Track ID'):
                continue
            track_info = {}
            node_list = ['Name', 'Artist', 'Album']
            for item in node_list:
                track_info[item.lower()] = lookup(elem, item)
            songs.append(track_info)
        return songs

    def parse_songs_csv(self, string):
        """Parses *.txt iTunes playlist which is actually \t delimited csv."""
        songs = []
        csvfile = StringIO(string, newline='')
        reader = csv.DictReader(csvfile, delimiter='\t')
        for row in reader:
            track_info = {}
            track_info['name'] = row['Name']
            track_info['artist'] = row['Artist']
            track_info['album'] = row['Album']
            songs.append(track_info)
        return songs
