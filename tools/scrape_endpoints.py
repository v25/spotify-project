import requests
import re
import os.path
import json

project_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
static_path = os.path.join(project_dir, 'music_app/static')

r = requests.get(
    'https://developer.spotify.com/documentation/web-api/reference-beta/')
file = r.text
pattern = r"<h2 id=.+>(.+)<\/h2>\n*.+\n*.*<code>([A-Z]+)"
pattern = pattern + r"\s(https:\/\/api\.spotify\.com\/v1\/.+)"
eps = re.findall(pattern, file)
endpoints = []

# Fixes miscoded apostrophe
for ep in eps:
    ep = list(ep)
    ep[0] = re.sub(r"â\x80\x99", "'", ep[0])
    endpoints.append(ep)

endpoints.sort()
# print(endpoints)

# Create short_names
for ep in endpoints:
    short_name = ep[0].replace("'s ", "-")
    short_name = short_name.replace(" a ", "-")
    short_name = short_name.replace(" an ", "-")
    short_name = short_name.replace(" the ", "-")
    short_name = short_name.replace(" and ", "+")
    short_name = short_name.replace(" or ", "/")
    short_name = short_name.replace(" ", "-")
    short_name = short_name.lower()
    ep.append(short_name)

# Create a dictionary from the list
d = {}
for ep in endpoints:
    d[ep[3]] = {}
    d[ep[3]]['name'] = ep[0]
    d[ep[3]]['method'] = ep[1]
    d[ep[3]]['url'] = ep[2]

# print(d)

# Write dictionary to a json file
with open(os.path.join(static_path, 'JSON/endpoints.json'), 'w') as f:
    json.dump(d, f, indent=4)
