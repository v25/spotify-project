import pytest
import os.path
import sys
import json

path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if path not in sys.path:
    sys.path.append(path)

from music_app import create_app, db  # noqa
from music_app.models import User  # noqa
from music_app.routes import ProfileData  # noqa


@pytest.fixture
def client():

    app = create_app()
    app.config['TESTING'] = True
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
    app.config['WTF_CSRF_ENABLED'] = False  # Redirects fail without this.

    client = app.test_client()

    with app.app_context():
        yield client


@pytest.fixture
def init_db():
    db.create_all()
    u1 = User(username='user1')
    u1.set_password('testtest')
    db.session.add(u1)
    db.session.commit()

    yield db

    db.session.remove()  # Not sure if this is necessary
    db.drop_all()


class MockResponse:
    @staticmethod
    def profile_response():
        with open(os.path.join(path, 'music_app/static/JSON/mock.json')) as f:
            response = json.load(f)
        return response


@pytest.fixture
def mock_profile_data(monkeypatch):
    def mock_profile(*args, **kwargs):
        return MockResponse.profile_response()["profile"]

    def mock_playlists(*args, **kwargs):
        return MockResponse.profile_response()["playlists"]

    def mock_history(*args, **kwargs):
        return MockResponse.profile_response()["history"]

    def mock_top_artists(*args, **kwargs):
        return MockResponse.profile_response()["top_artists"]

    def mock_top_tracks(*args, **kwargs):
        return MockResponse.profile_response()["top_tracks"]

    def mock_header(*args, **kwargs):
        return {'access_token': 'test', 'token_type': 'test'}

    monkeypatch.setattr(ProfileData, "profile_", mock_profile)
    monkeypatch.setattr(ProfileData, "playlists_", mock_playlists)
    monkeypatch.setattr(ProfileData, "history_", mock_history)
    monkeypatch.setattr(ProfileData, "top_artists_", mock_top_artists)
    monkeypatch.setattr(ProfileData, "top_tracks_", mock_top_tracks)
    monkeypatch.setattr(ProfileData, "mock_token_header", mock_header)
