import pytest


@pytest.mark.parametrize(
    "input, output",
    [
        (
            {'username': "test", 'password': "test", 'password2': "test"},
            b'must be at least 8 characters long.'
        ),
        (
            {'username': "test", 'password': "testtest", 'password2': "test"},
            b'must be equal to password.'
        ),
        (
            {
                'username': "user1",
                'password': "testtest",
                'password2': "testtest"
            },
            b'That name is already in use.'
        ),
    ]
)
def test_register_validators(client, init_db, input, output):
    response = client.post('auth/register', data=input)
    assert output in response.data


@pytest.mark.parametrize(
    "input, output",
    [
        (
            {'username': 'user1', 'password': 'catmouse'},
            b'Incorrect password'
        ),
        (
            {'username': 'george', 'password': 'catmouse'},
            b'That username is not registered.'
        )
    ]
)
def test_login_validators(client, init_db, input, output):
    response = client.post('auth/login', data=input)
    assert output in response.data
